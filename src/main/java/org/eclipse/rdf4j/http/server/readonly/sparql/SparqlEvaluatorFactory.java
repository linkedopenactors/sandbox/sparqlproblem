package org.eclipse.rdf4j.http.server.readonly.sparql;

public class SparqlEvaluatorFactory {
	public static final SparqlQueryEvaluator createSparqlQueryEvaluator() {
		return new SparqlQueryEvaluatorDefault();
	}

}
