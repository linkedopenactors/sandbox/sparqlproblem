package org.linkedopenactors.test;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.eclipse.rdf4j.http.server.readonly.sparql.EvaluateResult;
import org.eclipse.rdf4j.http.server.readonly.sparql.SparqlEvaluatorFactory;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.springframework.core.io.ClassPathResource;

public class Test {

	public static void main(String[] args) throws IOException {
		InputStream inputStream = getDataFromFile();
		Repository repo = createAndFillRepo(inputStream);

		EvaluateResult evaluateResult = new EvaluateResultDefault(new ByteArrayOutputStream());
		String queryString = "SELECT DISTINCT * WHERE {?subject <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>/rdfs:subClassOf* <https://www.w3.org/ns/activitystreams#Object>  . } ";

//		String[] namedGraphUris = new String[] {};
		String[] namedGraphUris = new String[] {
		    	"http://example.org/camel/0815",
		    	"https://schema.org/schemaorg-all-https.ttl_type",
		    	"http://example.org/camel/e9a8fe87-a43b-4d72-870f-265e36192ac1",
		    	"https://www.w3.org/ns/activitystreams#activitystreams-definitions.ttl_type",
		    	"http://example.org/camel/0815/outbox",
		    	"https://www.w3.org/ns/activitystreams#Public",
		    	"https://linkedopenactors.org/rdf-type-context"
		    };

		String defaultGraphUri = null;

		new SparqlEvaluatorFactory().createSparqlQueryEvaluator().evaluate(evaluateResult, repo, queryString, null,
				defaultGraphUri, namedGraphUris);

		System.out.println("ContentType: " + evaluateResult.getContentType());

		ByteArrayOutputStream stream = (ByteArrayOutputStream) evaluateResult.getOutputstream();
		System.out.println(new String(stream.toByteArray()));
	}

	private static Repository createAndFillRepo(InputStream inputStream) throws IOException {
		Repository repo = new SailRepository(new MemoryStore());
		Model m = Rio.parse(inputStream, RDFFormat.TRIG);
		try (RepositoryConnection con = repo.getConnection()) {
			con.add(m);
			con.getContextIDs().forEach(System.out::println);
		}
		return repo;
	}

	private static InputStream getDataFromFile() throws IOException {
		ClassPathResource cpr = new ClassPathResource("dump.ttl");
		InputStream inputStream = cpr.getInputStream();
		if (inputStream == null) {
			throw new IllegalStateException("no stream");
		}
		return inputStream;
	}

	public String readFromInputStream(InputStream inputStream) throws IOException {
		StringBuilder resultStringBuilder = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
			String line;
			while ((line = br.readLine()) != null) {
				resultStringBuilder.append(line).append("\n");
			}
		}
		return resultStringBuilder.toString();
	}
}
